<?php
/**
 * 2007-2020 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2020 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

class ProductDiscountRequest extends ObjectModel
{
    public $dateOfRequest;
    public $userName;
    public $userEmail;
    public $message;
    public $product_id;
    public $product_name;


    public static $definition = array(
        'table' => 'gsproductdiscountrequest',
        'primary' => 'id_gsproductdiscountrequest',
        'multilang' => false,
        'fields' => array(
            'dateOfRequest' => array('type' => self::TYPE_DATE,'valudate' => 'isDate'),
            'userName' =>        array('type' => self::TYPE_STRING,'validate' => 'isName'),
            'userEmail' =>        array('type' => self::TYPE_STRING,'validate' => 'isEmail'),
            'message' =>        array('type' => self::TYPE_STRING,'validate' => 'isCleanHtml'),
            'product_id' =>        array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'product_name' => array('type' => self::TYPE_STRING)
        ),
    );

//    public function insertValuesToDatabase($dateOfRequest, $userName, $userEmail, $message, $productId)
//    {
//             $result =  Db::getInstance()->insert("gsproductdiscountrequest", array(
//                 'userName' => (string) $userName,
//                 'userEmail' => (string) $userEmail,
//                 'dateOfRequest' => $dateOfRequest,
//                 'message' => (string) $message,
//                 'product_id' => (int) $productId
//             ));
//    }
}
