<?php
/**
 * 2007-2020 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2020 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

use Doctrine\Common\Cache\CacheProvider;
use PrestaShopBundle\Controller\Admin\FrameworkBundleAdminController;

include_once('C:\laragon\www\prestashop1767\modules\gsproductdiscountrequest\classes\ProductDiscountRequest.php');

class AdminDiscountRequestsController extends ModuleAdminController
{
    public function __construct()
    {
        $this->table = 'gsproductdiscountrequest';
        $this->className = 'ProductDiscountRequest';
        $this->fields_list = array(
            'id_gsproductdiscountrequest' => array('title' => 'ID', 'align' =>
                'center', 'width' => 25),
            'userName' => array('title' => 'Username', 'width' =>
                120),
            'userEmail' => array('title' => 'User Email', 'width' =>
                140),
            'dateOfRequest' => array('title' => 'Date of Request', 'width' => 150),
            'message' => array('title' => 'Message', 'align' =>
                'right', 'width' => 80),
            'product_id' => array('title' => 'Product ID', 'search' =>
                false),
            'product_name' => array('title'=> 'Product Name','search' => false)
        );
        $this->addRowAction('delete');
        $this->addRowAction('edit');
        $this->addRowAction('view');
// Enable bootstrap
        $this->bootstrap = true;
// Call of the parent constructor method
        parent::__construct();
    }

    public function getProducts()
    {
        $products = Product::getProducts(Context::getContext()->language->id, 0, 0, 'name', 'ASC');

        return $products  ;
    }


    public function renderView()
    {
        $this->meta_title = $this->l('Discount request', 'AdminDiscountRequestsController');
        $this->toolbar_title[] = $this->meta_title;
        $this->page_header_toolbar_title = $this->meta_title;


        $this->context->smarty->assign(['userName' => $this->object->userName,
            'userEmail' => $this->object->userEmail,
            'requestDate' => $this->object->dateOfRequest,
            'message' => $this->object->message,
            'productId' => $this->object->product_id,
            'id' => $this->object->id,
            'product_name' => $this->object->product_name]);

        return $this->context->smarty->fetch('module:'.$this->module->name.'/views/templates/admin/adminView.tpl');
    }

    public function renderForm()
    {
            $this->fields_form = array(
                'legend' => array(
                    'title' => $this->trans('Edit discount request', array(), 'Admin.Catalog.Feature'),
                    'icon' => 'icon-paper-clip',
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->trans('Username', array(), 'Admin.Global'),
                        'name' => 'userName',
                        'required' => true,
                        'col' => 4,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->trans('UserEmail', array(), 'Admin.Global'),
                        'name' => 'userEmail',

                        'col' => 6,
                    ),
                    array(
                        'type' =>  'select',
                        'label' => 'Select a product',
                        'name' => 'product_id',
                        'required' => true,
                        'options' => array(
                            'query' => $this->getProducts(),
                            'id' => 'id_product',
                            'name' => 'name',
                        )
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => $this->trans('Message', array(), 'Admin.Global'),
                        'name' => 'message',
                        'col' => 6
                    ),
                ),
                'buttons' => array(
                    'save-and-stay' => array(
                        'title' => $this->l('Save and Stay'),
                        'name' => 'submitAdd'.$this->table.'AndStay',
                        'type' => 'submit',
                        'class' => 'btn btn-default pull-right',
                        'icon' => 'process-icon-save',
                    ),
                ),
            );

            if (false !== Tools::getValue('add'.$this->table)) {
                $this->fields_form['input'][] = ['type' => 'hidden', 'name' => 'add'.$this->table];
            }
                $this->fields_value = $this->getFieldsFormValues();

            return parent::renderForm();
    }



    public function init()
    {
        parent::init(); // TODO: Change the autogenerated stu
    }

    public function postProcess()
    {

        if (Tools::isSubmit("submitAddgsproductdiscountrequestAndStay")) {
            if (!Validate::isName(Tools::getValue("userName"))) {
                $this->context->controller->errors[] = "Name is not valid please try again";
                return false;
            }
            if (!Validate::isEmail(Tools::getValue('userEmail'))) {
                $this->context->controller->errors[] = "Email is not valid please try again";
                return false;
            }
            if (!Validate::isCleanHtml(Tools::getValue("message"))) {
                $this->context->controller->errors[] = "The message you have written is not valid please try again";
                return false;
            }
        }
        if (Tools::isSubmit('submitAdd'.$this->table)) {
            if (Tools::getValue('id_gsproductdiscountrequest')) {
                $id_gsDiscountRequest = Tools::getValue('id_gsproductdiscountrequest');
                $gsDiscountRequest = new ProductDiscountRequest((int)$id_gsDiscountRequest);

                $gsDiscountRequest->userName = Tools::getValue('userName');
                $gsDiscountRequest->userEmail = Tools::getValue('userEmail');
                $gsDiscountRequest->message = Tools::getValue("message");
                $gsDiscountRequest->product_id = Tools::getValue('product_id');
                $gsDiscountRequest->product_name = Product::getProductName(Tools::getValue('product_id'));
                $gsDiscountRequest->dateOfRequest = date('Y-m-d h:i:s');

                if ($gsDiscountRequest->update()) {
                    return $this->context->controller->confirmations[] = $this->module->l(
                        'Discount request successfully saved.'
                    );
                }
            } else {
                $gsDiscountRequest = new ProductDiscountRequest();
                $gsDiscountRequest->userName = Tools::getValue('userName');
                $gsDiscountRequest->userEmail = Tools::getValue('userEmail');
                $gsDiscountRequest->message = Tools::getValue("message");
                $gsDiscountRequest->product_id = Tools::getValue('product_id');
                $gsDiscountRequest->product_name = Product::getProductName(Tools::getValue('product_id'));
                $gsDiscountRequest->dateOfRequest = date('Y-m-d h:i:s');
                if ($gsDiscountRequest->add()) {
                    return $this->context->controller->confirmations[] = $this->module->l(
                        'New discount request successfully added'
                    );
                }
            }
        }
        return parent::postProcess();
    }

    protected function getFieldsFormValues()
    {
        $fields = [];
        if (Tools::isSubmit('id_gsproductdiscountrequest')) {
            $gsdiscountData= new ProductDiscountRequest((int) Tools::getValue('id_gsproductdiscountrequest'));
            $fields['id_gsfiltersseodata'] = (int) Tools::getValue('id_gsproductdiscountrequest');
        } else {
            $gsdiscountData = new ProductDiscountRequest();
        }
        $fields['userName'] = Tools::getValue('userName', $gsdiscountData->userName);
        $fields['userEmail'] = Tools::getValue('userEmail', $gsdiscountData->userEmail);
        $fields['dateOfRequets'] = Tools::getValue('dateOfRequest', $gsdiscountData->dateOfRequest);
        $fields['message'] = Tools::getValue('message', $gsdiscountData->message);
        $fields['product_id'] = Tools::getValue('product_id', $gsdiscountData->product_id);

        return $fields;
    }
}
