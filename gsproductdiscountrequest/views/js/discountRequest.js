/**
 * 2007-2020 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2020 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

$(document).ready(function (){
    $(document).on('click','button#DiscountFormButton',function(e){
        e.preventDefault();
        $("#errors").empty().hide();
        $("#errors").removeClass();
        console.log(UrlToController);

        var userName = $('input[name=user_name]#userName').val();
        var userEmail = $('input[name=email]#userEmail').val();
        var message = $('textarea#message').val();
        var idProduct = $('select#productsForDiscount').val();
        var checkedBox = $('input[name=terms]#termsAndConditions').is(':checked') ? true : false;
        var product_name = $('select#productsForDiscount').find(':selected').text();

        var request_data = {
                user_name : userName,
                user_email : userEmail,
                product_id : idProduct,
                terms : checkedBox,
                message : message,
                product_name : product_name
        }

        $.ajax({
            type:"POST",
            url:UrlToController+"?gsdebug=1",
            data:request_data,
            success: function (data,response,xhr)
            {
                var result  = JSON.parse(data);

                $.each(result.errors,function (key,value){

                    $("#errors").addClass("alert alert-danger");
                    $("#errors").append('<p>'+ value + '</p>')

                });
                $.each(result.success,function (key,value){

                    console.log(value);
                    $("#errors").addClass("alert alert-success");
                    $("#errors").append('<p>'+ value + '</p>')

                });
                $("#errors").show().delay(4000).hide(1000);
            },
        });
    });
});






